## BitLvL - IoT Progressive WebApp


```bash
npm install
```

Then, to run it, cd into the repository, and run platform specific build

```bash
$ ionic cordova platform add ios *(or android)*
$ ionic cordova run ios *(or android)*
```

Or run the webapp version on your browser:
```bash
npm start
```
- Port is 8100 by default
- To simulate all three versions, go to **localhost:8100/ionic-lab**