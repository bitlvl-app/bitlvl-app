Version 0.1.3
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.1.3] - 2017-10-23
### Removed
- Removing automatic changelog tool.
### Change
- For now on, the changelog will be maintained manually.

## [0.1.2] - 2017-10-22
### Added
- Adding Angular HTTP to handle requests
### Removed
- Removing axios library

## [0.1.1] - 2017-10-22
### Added
- Initial project setup.