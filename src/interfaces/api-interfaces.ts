interface DeviceSummary {
    name: string,
    task: string,
    status: string
}

interface APIResponse {
    status: Number,
    message: any
}

export { DeviceSummary, APIResponse }