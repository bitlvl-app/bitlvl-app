import { Injectable } from '@angular/core'
import { HttpReq } from "../utils"
import { Observable } from 'rxjs/Observable'

@Injectable()
export default class LoginService {

    private http: HttpReq

    constructor(http: HttpReq) {
        this.http = http
    }

    public registerRequest(formData: Object): Observable<any> {
        return this.http.XHRpost('users', formData)
    }

    public loginRequest(formData: Object) {
        return this.http.XHRpost('auth', formData)
    }

    public getCountries() {
        return this.http.XHRget('countries')
    }

    public getRoles() {
        return this.http.XHRget('roles')
    }
}