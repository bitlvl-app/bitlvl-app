import { Component } from '@angular/core';
import { NavController, LoadingController, Loading } from 'ionic-angular';
import { DeviceSummary } from '../../interfaces'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  private alerts: Array<DeviceSummary>
  private loading: Loading

  constructor(navCtrl: NavController, loadCtrl: LoadingController) {
    this.loading = this.loadingExample(loadCtrl)
    this.getSensors();
  }

  loadingExample(loadCtrl: LoadingController) {
    const myLoading = loadCtrl.create({
      content: 'Wait a sec...'
    })

    myLoading.present()

    return myLoading
  }

  getSensors() {
    this.alerts = []
    this.loading.dismiss()
  }
}
