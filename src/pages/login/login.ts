import { Component } from '@angular/core'
import { MenuController, NavController, NavParams } from 'ionic-angular'
import { HomePage } from '../home/home'
import { LoginService } from '../../services'
import { APIResponse } from '../../interfaces'
import { TextConstants } from '../../constants'

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [LoginService]
})
export class LoginPage {

  private showRegister: boolean
  private showLogin: boolean
  private registerForm: Object
  private loginForm: Object
  private countryOptions: Array<Object>
  private roleOptions: Array<Object>
  private registerError: String
  private response: APIResponse

  constructor(
    public navCtrl: NavController,
    public reqProv: LoginService,
    public navParams: NavParams,
    public menuCtrl: MenuController
  ) {
    menuCtrl.swipeEnable(false, 'main-menu')
    this.showRegister = false
    this.showLogin = false
    this.registerForm = {}
    this.loginForm = {}
  }

  openRegister(): void {
    this.showRegister = true
    this.showLogin = false
    this.getCountries()
    this.getRoles()
  }

  openLogin(): void {
    this.showLogin = true
    this.showRegister = false
  }

  loginRequest(){
    this.reqProv.loginRequest(this.loginForm).forEach(data => {
      this.loginHandler(data)
    }).catch(err => {
      this.loginHandler(err)
    })
  }

  registerRequest() {
    this.reqProv.registerRequest(this.registerForm).forEach(data => {
      this.registerHandler(data)
    }).catch(err => {
      this.registerHandler(err)
    })
  }

  registerHandler(data) {
    this.response = {
      status: data.status,
      message: data.json().message
    }
    this.response.status === 200 ? this.onSucess() : this.onRegisterError(this.response.message.code)
  }

  onRegisterError(error): void {
    if (error === 11000) {
      this.registerError = TextConstants.DUPLICATE_EMAIL_ERROR
    } else {
      this.registerError = TextConstants.GENERIC_ERROR
    }
  }

  loginHandler(data): void {
    this.response = {
      status: data.status,
      message: data.json().message
    }
    this.response.status === 200 ? this.onSucess() : this.onRegisterError(this.response.message.code)
  }

  getCountries(): void {
    this.reqProv.getCountries().forEach(data => {
      this.countryOptions = data.json()
    })
  }

  getRoles(): void {
    this.reqProv.getRoles().forEach(data => {
      this.roleOptions = data.json()
    })
  }

  onSucess(): void {
    this.navCtrl.setRoot(HomePage)
  }

}
