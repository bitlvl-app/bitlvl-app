

const TextConstants = {
    DUPLICATE_EMAIL_ERROR: `Email já cadastrado!`,
    GENERIC_ERROR: `Ocorreu um erro. Tente novamente mais tarde`
}

export default TextConstants