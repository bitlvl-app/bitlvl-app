import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import mainConfig from '../config/main.config';

@Injectable()
export default class HttpReq {
    private http: Http

    constructor(http: Http) {
        this.http = http;
    }

    public XHRpost(endpoint: string, data: Object) {
        return this.http.post(
            `${mainConfig.rootUrl}/${endpoint}`,
            data
        )
    }

    public XHRget(endpoint: string) {
        return this.http.get(
            `${mainConfig.rootUrl}/${endpoint}`,
        )
    }
}